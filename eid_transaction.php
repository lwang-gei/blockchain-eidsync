<?php

class Eid_transaction{

	private $txn_id;
	private $eagleId;
	private $activity_code;
	private $txn_location;
	private $txn_country;
	private $activity_notes;
	private $txn_date;
	private $organization;
	private $internal_notes;
	private $file_uploaded;
	private $longitude;
	private $latitude;
	private $created_by;
	private $created_at;
	private $updated_at;
	
	
	
	function __construct($row){
		$txn_id = $row['txn_id'];
		$eagleId = $row['eagleid'];
		$activity_code = $row['activity_code'];
		$txn_location = $row['txn_location'];
		$txn_country = $row['txn_country'];
		$activity_notes = $row['activity_notes'];
		$txn_date = $row['txn_date'];
		$organization = $row['organization'];
		$internal_notes = $row['internal_notes'];
		$file_uploaded = $row['file_uploaded'];
		$longitude = $row['longitude'];
		$latitude = $row['latitude'];
		$created_by = $row['created_by'];
		$created_at = $row['created_at'];
		$updated_at = $row['updated_at'];
	}
	

}

?>