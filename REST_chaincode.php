<?php
require_once 'CurlClient.php';

class REST_chaincode{

	private $url;
	private $chaincodeId;
	private $userId;
	private $userSecret;
	
	function __construct($chaincodeId,$url,$userId,$userSecret){
		$this->chaincodeId = $chaincodeId;
		$this->url = $url;
		$this->userId = $userId;
		$this->userSecret = $userSecret;
	}

	public function LoginChaincode(){
		$httpClient = CurlClient::instance();
		
	  	$params = array("enrollId" => $this->userId,
  						"enrollSecret"=> $this->userSecret
  						);
  		
		list($rbody, $rcode, $rheaders) = $httpClient->request("post",$url,null,$params,false);

	}

	public function DeployChaincode(){
		$httpClient = CurlClient::instance();
		
		if($this->chaincodeId!=null)return true;
		
		$params = array("jsonrpc" => "2.0",
						"method" => "deploy",
						"params"=> array("type" => 1
										,"chaincodeID" => array("path" => "https://github.com/wangleitj77/learn-chaincode/finished")
										,"ctorMsg" => array("function"=> "init","args"=>array("hi chaincode user_type1_1") )
										,"secureContext" => $this->userId
										),
						"id" => 1
						);

		list($rbody, $rcode, $rheaders) = $httpClient->request("post",$url,null,$params,false);
		
	}
	
	public function InvokeChaincode(){
	
	
	}
	
	public function QueryChaincode(){
	
	}

}

?>