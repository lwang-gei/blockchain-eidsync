package main

import (
	"errors"
	"fmt"
	"strconv"
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

var eidIndexStr = "_eidindex"				//name for the key/value that will store a list of all known eagleitem

type EagleItem struct{
	eagleid string `json:"eagleid"`
	txn_id int `json:"txn_id"`
	txn_location string `json:"txn_location"`
	txn_country string `json:"txn_country"`
	txn_date string `json:"txn_date"`
	organization string `json:"organization"`
	longitude string `json:"longitude"`
	latitude string `json:"latitude"`
	created_by string `json:"created_by"`
	updated_by string `json:"updated_by"`
	updated_at string `json:"updated_at"`
	shipping_trans []string `json:"shipping_trans"`
}


// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// ============================================================================================================================
// Init - reset all the things
// ============================================================================================================================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	var Aval int
	var err error

	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	// Initialize the chaincode
	Aval, err = strconv.Atoi(args[0])
	if err != nil {
		return nil, errors.New("Expecting integer value for asset holding")
	}

	// Write the state to the ledger
	err = stub.PutState("abc", []byte(strconv.Itoa(Aval)))				//making a test var "abc", I find it handy to read/write to it right away to test the network
	if err != nil {
		return nil, err
	}
	
	var empty []string
	jsonAsBytes, _ := json.Marshal(empty)								//marshal an emtpy array of strings to clear the index
	err = stub.PutState(eidIndexStr, jsonAsBytes)
	if err != nil {
		return nil, err
	}
	
	return nil, nil
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// ============================================================================================================================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "init" {													//initialize the chaincode state, used as reset
		return t.Init(stub, "init", args)
	} else if function == "write" {											//writes a value to the chaincode state
		return t.Write(stub, args)
	} else if function == "add_shipping_trans" {							//add a block uuid for last shipping transaction
		return t.AddShippingTrans(stub, args)
	}
	fmt.Println("invoke did not find func: " + function)					//error

	return nil, errors.New("Received unknown function invocation")
}



// ============================================================================================================================
// Query - Our entry point for Queries
// ============================================================================================================================
func (t *SimpleChaincode) Query(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	fmt.Println("query is running " + function)

	// Handle different functions
	if function == "read" {													//read a variable
		return t.read(stub, args)
	}
	fmt.Println("query did not find func: " + function)						//error

	return nil, errors.New("Received unknown function query")
}

// ============================================================================================================================
// Read - read a variable from chaincode state
// ============================================================================================================================
func (t *SimpleChaincode) read(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	var name, jsonResp string
	var err error

	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting name of the var to query")
	}

	name = args[0]
	valAsbytes, err := stub.GetState(name)									//get the var from chaincode state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	return valAsbytes, nil													//send it onward
}

// ============================================================================================================================
// Write - write variable into chaincode state
// ============================================================================================================================
func (t *SimpleChaincode) Write(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	var name, value string // Entities
	var err error
	fmt.Println("running write()")

	if len(args) != 2 {
		return nil, errors.New("Incorrect number of arguments. Expecting 2. name of the variable and value to set")
	}

	name = args[0]															//rename for funsies
	value = args[1]
	//check if eagleItem already exists
	eagleItemAsBytes, err := stub.GetState(name)
	if err == nil {
		//fund
		//parse eagle struct
		res := EagleItem{}
		json.Unmarshal(eagleItemAsBytes, &res)
		
		//shipping_trans should use previous one
		newItem := EagleItem{}
		json.Unmarshal([]byte(value), &newItem)
		newItem.shipping_trans = res.shipping_trans;
		
		valueAsByte, _ := json.Marshal(newItem)
		err = stub.PutState(name, []byte(valueAsByte))								//write the variable into the chaincode state
		if err != nil {
			return nil, err
		}
		
	}else{
		//not found eagle item, add eagleindex
		eagleAsBytes, err := stub.GetState(eidIndexStr)
		if err != nil {
			return nil, errors.New("Failed to get eagleintem index")
		}
		var eidIndex []string
		json.Unmarshal(eagleAsBytes, &eidIndex)							//un stringify it aka JSON.parse()
	
		//append
		eidIndex = append(eidIndex, name)									//add eagleid  to index list
		fmt.Println("! eidIndex index: ", eidIndex)
		jsonAsBytes, _ := json.Marshal(eidIndex)
		err = stub.PutState(eidIndexStr, jsonAsBytes)						//store name of marble
		if err != nil {
			return nil, err
		}
		err = stub.PutState(name, []byte(value))								//write the variable into the chaincode state
		if err != nil {
			return nil, err
		}
	}
	return nil, nil
}

// ============================================================================================================================
// addShippingTrans - set a block uuid for last shipping transaction
// ============================================================================================================================
func (t *SimpleChaincode) AddShippingTrans(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	var err error

	if len(args) != 2 {
		return nil, errors.New("Incorrect number of arguments. Expecting 2")
	}

	//input sanitation
	fmt.Println("- start init marble")
	if len(args[0]) <= 0 {
		return nil, errors.New("1st argument must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return nil, errors.New("2nd argument must be a non-empty string")
	}

	name := args[0]
	uuid := args[1]

	//check if eagleShippingTrans already exists
	eagleItemAsBytes, err := stub.GetState(name)
	if err != nil {
		return nil, errors.New("Failed to get eagleid ")
	}
	
	//parse eagle struct
	res := EagleItem{}
	json.Unmarshal(eagleItemAsBytes, &res)
	
	//add new uuid to shipping trans
	res.shipping_trans = append(res.shipping_trans, uuid)					
				
	jsonAsBytes, _ := json.Marshal(res)
	err = stub.PutState(name, jsonAsBytes)						
	
	fmt.Println("- end AddShippingTrans")
	return nil, nil
}






