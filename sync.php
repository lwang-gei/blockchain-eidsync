<?php
require_once 'Error/Base.php';
require_once 'eid_transaction.php';
require_once 'REST_chaincode.php';


function writelog($msg){
	date_default_timezone_set('America/Los_Angeles');
	$date = date('m/d/Y h:i:s a', time());
	
    $myfile = fopen('eid.log', "a") or die("Unable to open log file!");
    $txt = "[$date]" . $msg . "\n";
    print_r($txt);
	fwrite($myfile, $txt);
	fclose($myfile);
	
}


//read config file
$ini_array = parse_ini_file('eid.cfg.ini');
//print_r($ini_array);

$dbservername = $ini_array["dbservername"];
$username = $ini_array["username"];
$password = $ini_array["password"];
$dbname = $ini_array["dbname"];
$userId = $ini_array["userId"];
$userSecret = $ini_array["userSecret"];

$eidcfg_array = parse_ini_file('eid.txn.ini');
$maxEID = $eidcfg_array["maxEID"];
$maxEID = is_numeric($maxEID)?$maxEID:0;
$chaincodeID = $eidcfg_array["chaincodeID"];

// Create db connection
$conn = new mysqli($dbservername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM tracking_txn where txn_id>" . $maxEID . " order by txn_id";
$result = $conn->query($sql);

if ($result!=null && $result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
    	if($maxEID<$row["txn_id"])$maxEID = $row["txn_id"];
    	
    	$eTrans = new Eid_transaction($row);
    	writelog(print_r($row,true));
    	//call chiancode to append transaction
    }
     
    //write maxEID
    $myfile = fopen('eid.txn.ini', "w") or die("Unable to open file!");
    $txt = "maxEID=" . $maxEID . "\n";
    $txt .= "chaincodeID=" . $chaincodeID . "\n";
    writelog(print_r($txt));
	fwrite($myfile, $txt);
	fclose($myfile);
	
} else {
    writelog( "0 results");
}
$conn->close();




?>